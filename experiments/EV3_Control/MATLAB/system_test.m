% init the connection
disp('Connecting ... ')
% brick usb init
%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
b = Brick('ioType','usb');
% beep to indicate connection
b.playThreeTones();

% user intput
userIn = '';

%reset routine
b.outputClrCount(0,Device.MotorB)
b.outputClrCount(0,Device.MotorD)

while(~strcmp(userIn,'q'))
    % get input
    userIn = input('> Input(s, d, r, l, q): ', 's');

    if (userIn == 'm')
        playMarioTheme(b,100);
    end
    
    if (userIn == 'd')
        reading = b.inputReadSI(0,Device.Port2,Device.Ultrasonic);
        reading
    end
    % turn left
    if (userIn == 'l')
        b.outputStepSpeed(0,Device.MotorB,-30,0,240,0,Device.Brake)
        pause(0.001)
        b.outputStepSpeed(0,Device.MotorC,30,0,240,0,Device.Brake)
        disp('> Left');
        tachoD = b.outputGetCount(0,Device.MotorB);
        disp(['> Tachometer: ' num2str(tachoD)]);
    end
    
    % turn right
    if (userIn == 'r')
        b.outputStepSpeed(0,Device.MotorB,30,0,240,0,Device.Brake)
        pause(0.001)
        b.outputStepSpeed(0,Device.MotorC,-30,0,240,0,Device.Brake)
        disp('> Right');
        tachoD = b.outputGetCount(0,Device.MotorC);
        disp(['> Tachometer: ' num2str(tachoD)]);
    end
    
    % measure distance
    if (userIn == 'r')
        b.outputStepSpeed(0,Device.MotorB,30,0,240,0,Device.Brake)
        disp('> Distance');
        tachoD = b.outputGetCount(0,Device.MotorC);
        disp(['> Tachometer: ' num2str(tachoD)]);
    end
end
% delete the brick object
delete(b)
clear