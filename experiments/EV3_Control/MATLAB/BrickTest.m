% init the connection
disp('Connecting ... ')
% brick usb init
%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
b = Brick('ioType','usb');
% beep to indicate connection
b.beep();
%reset routine
b.outputClrCount(0,Device.MotorA);
pause(0.001);
b.outputClrCount(0,Device.MotorB);
pause(0.001);
b.outputClrCount(0,Device.MotorC);
pause(0.001);

white=40;
black=4;
%main routine 
%fprintf('Learning phase! Please place me on the white and press any key. \n');
%pause 
%white = b.inputReadSI(0,Device.Port1,Device.ColReflect)
%fprintf('Now place me on the black and press ENTER. \n');
%pause 
%black = b.inputReadSI(0,Device.Port1,Device.ColReflect)


lineLen=FollowBlackLine(b,white,black,30,5) % 5 meter max

b.outputStepSpeed(0,Device.MotorB,-10,0,60,0,Device.Coast);
pause(0.001);
b.outputStepSpeed(0,Device.MotorC,-10,0,60,0,Device.Coast);
pause(2);

WaitButtonPressed(b);
lineLen=FollowBlackLine(b,white,black,-30,lineLen/2) % 5 meter max

%RotateAndSing(b);
% delete the brick object
delete(b)
clear