
function [ distanceTraveled ] = GetBlackLine( b, black, maxspeed, max_travel)
    WHEEL_DIAMETER=0.056;
    buttonPressed=0;
    b.outputPower(0,Device.MotorB, maxspeed);
    pause(0.1);
    b.outputPower(0,Device.MotorC, maxspeed);
    pause(0.1);
    b.outputStart(0,Device.MotorB+Device.MotorC);
    pause(0.1);

    intensity=black+2;
    initialTacho=double(b.outputGetCount(0,Device.MotorB));
    tachoB=0;
    distanceTraveled=0;
    while(buttonPressed==0 && intensity>black && (distanceTraveled<max_travel))
        buttonPressed=b.inputReadSI(0,Device.Port4,Device.Pushed);
        pause(0.1);
        intensity=b.inputReadSI(0,Device.Port1,Device.ColReflect)
        pause(0.1);
        
        leftSpeed=maxspeed;
        rightSpeed=maxspeed;
        
        tachoB=double(b.outputGetCount(0,Device.MotorB));
        %pause(0.01);
        distanceTraveled=abs(((tachoB-initialTacho)/360)*pi*WHEEL_DIAMETER)
        
        %if (intensity==black)
        %    leftspeed = maxspeed;
        %    rightSpeed = maxspeed;
        %else        
        
        disp('leftspeed=');
        disp(leftSpeed);
        disp('rightspeed=');
        disp(rightSpeed);
        b.outputPower(0,Device.MotorB, leftSpeed);
        %pause(0.01);
        b.outputPower(0,Device.MotorC, rightSpeed);
        %pause(0.01);        
    end
    
    b.outputStop(0,Device.MotorB+Device.MotorC,0)
    pause(0.05);
    %tachoB=double(b.outputGetCount(0,Device.MotorB))
    %linelength=((initialTacho-tachoB)/360)*pi*WHEEL_DIAMETER 
end

