function [ output_args ] = ReachWall( b )
    DISTANCE_TO_WALL=20;
    DRIVESPEED=20;
    pause(0.1);
    b.outputPower(0,Device.MotorB, DRIVESPEED);
    pause(0.1);
    b.outputPower(0,Device.MotorC, DRIVESPEED);
    pause(0.1);

    b.outputStart(0,Device.MotorB+Device.MotorC);
    pause(0.01);

    distance=255;
    while(distance>DISTANCE_TO_WALL)
        distance = b.inputReadSI(0,Device.Port2,Device.Ultrasonic);
        pause(0.1);
    end
    b.outputStop(0,Device.MotorB+Device.MotorC,0)


end

