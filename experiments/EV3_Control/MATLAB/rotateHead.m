function [ output_args ] = rotateHead(b, angle, speed)
%reset routine
pause(0.1);
speed=sign(angle)*abs(speed);
angle=abs(angle);
b.outputStepSpeed(0,Device.MotorA,speed,0,angle,0,Device.Brake);
pause(0.1);
end

