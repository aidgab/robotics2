function [ finalAngle ] = findClosestWall( b )
%will search for the closest wall and return the azimuth
    STEP_ANGLE=30;
    ROTATE_SPEED=30;
    angle=-180;
    finalAngle = 0;
    rotateHead(b,angle, ROTATE_SPEED);
    pause(1);
    minDistance=255;
    while (angle<180)
        if (b.outputTest(0,Device.MotorA)==0)
            rotateHead(b,STEP_ANGLE, ROTATE_SPEED);
            pause(0.2);
            
            angle=angle+STEP_ANGLE;
            
            distance=b.inputReadSI(0,Device.Port2,Device.Ultrasonic)
            pause(0.05);
            %pause;
            if (distance < minDistance)
                finalAngle=angle;
                minDistance=distance;            
            end
            
        end
        pause(0.05);
    end
    
    %return read to the original position
    rotateHead(b,-180, ROTATE_SPEED);
    pause(1);
end

