function [ output_args ] = RotateChassis( b, angle, speed )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

    b.outputStepSpeed(0,Device.MotorB,sign(angle)*speed,0,abs(angle)*2,0,Device.Brake);
    pause(0.01);
    b.outputStepSpeed(0,Device.MotorC,-sign(angle)*speed,0,abs(angle)*2,0,Device.Brake);
    pause(1);
end

