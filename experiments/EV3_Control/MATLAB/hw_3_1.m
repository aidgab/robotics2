% init the connection
disp('Connecting ... ')
% brick usb init
%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
b = Brick('ioType','usb');
% beep to indicate connection
b.beep();

DRIVESPEED=30;
DISTANCE_TO_WALL = 20; %in cm+

%reset routine
b.outputClrCount(0,Device.MotorA);
b.outputClrCount(0,Device.MotorB);
b.outputClrCount(0,Device.MotorC);


%pause(0.5);
%rotateHead(b,-5, 30);
%pause(1);

pause(0.5);
azimuth=findClosestWall(b)
%b.beep();
pause(1);
%rotate to the azimuth
RotateChassis(b,azimuth,20);
pause(1);

ReachWall(b);
pause(1);

RotateChassis(b,90, 30);
pause(1);

%WaitButtonPressed(b);

rotateHead(b,-90, 30);
pause(1);

WallFollow(b, 1, 10);

%drive a little further
b.outputStepSpeed(0,Device.MotorB,50,0,240,0,Device.Brake);
pause(0.001);
b.outputStepSpeed(0,Device.MotorC,50,0,240,0,Device.Brake);
pause(2);
%rotate
RotateChassis(b,90, 30);
%WaitButtonP(b);
pause(0.1)

% drive back to follow find start of the line
%lineLen=FollowBlackLine(b,36,10,-30,0.2) % 5 meter max
%pause;
lineLen=FollowBlackLine(b,15,10,-15,5) % 5 meter max
pause
GetBlackLine(b,20,10, 1);
disp('black line is found');
pause(0.1);
lineLen=FollowBlackLine(b,20,10,15,5) % 5 meter max
disp('black line is finished');
pause
%WaitButtonPressed(b);
lineLen=FollowBlackLine(b,50,10,-30,lineLen/2) % 5 meter max
pause(0.1)

%return head to original point
headAngle=b.outputGetCount(0,Device.MotorA);
rotateHead(b,-headAngle, 30);
pause(1);

b.outputStepSpeed(0,Device.MotorA,5,0,0,0,Device.Coast);

pause(0.5);
% delete the brick object
delete(b)
clear