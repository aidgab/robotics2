function [ ] = WallFollow(b, watchLine, lineIntensity)
    % init the connection
    disp('Following the wall ... ')

    DRIVESPEED=30;
    MIN_DISTANCE_TO_WALL = 15; %in cm+
    MAX_DISTANCE_TO_WALL = 25; %in cm+

    pause(0.1);
    b.outputPower(0,Device.MotorB, DRIVESPEED);
    pause(0.1);
    b.outputPower(0,Device.MotorC, DRIVESPEED);
    pause(0.1);

    b.outputStart(0,Device.MotorB+Device.MotorC);
    pause(0.01);

    buttonPressed=0;
    %headDir=90;
    distance=0.0;
    distances=zeros(3);
    distances(1:3)=MIN_DISTANCE_TO_WALL;
    
    lineCrossed=0;
    P=0.2;
    i=0;
    steps2Check=10;
    while(buttonPressed==0 && ~lineCrossed)
        i=i+1
        if (b.outputTest(0,Device.MotorA)==0 && i>steps2Check)
           b.outputStop(0,Device.MotorB+Device.MotorC,Device.Coast)

           rotateHead(b,90,100);
           pause(0.1);
           distanceAhead=b.inputReadSI(0,Device.Port2,Device.Ultrasonic)
           if (distanceAhead<30)
               RotateChassis(b,60,30);
               steps2Check=10;
               pause(0.2);
           elseif (distanceAhead<255 && distanceAhead>100)
               steps2Check=30;
               %pause;
           else
               steps2Check=10;
           end
           rotateHead(b,-90,100);
           pause(0.1);           
           i=0;
           b.outputPower(0,Device.MotorB, DRIVESPEED);
            pause(0.1);
            b.outputPower(0,Device.MotorC, DRIVESPEED);
            pause(0.1);

            b.outputStart(0,Device.MotorB+Device.MotorC);
            pause(0.01);

        end

        %watch out the line
        if (watchLine==1)
            if (b.inputReadSI(0,Device.Port1,Device.ColReflect)<lineIntensity)
                lineCrossed=1
            end
        end
        %headAngle=double(b.outputGetCount(0,Device.MotorA))
        sonarRead=b.inputReadSI(0,Device.Port2,Device.Ultrasonic)
        
        distances=circshift(distances,1);
        distances(1)=sonarRead;

%        if (headAngle<=-40)
%            distances=circshift(distances,1);
%            distances(1)=sonarRead;
            %distance = abs(sin(headAngle*pi/180)*sonarRead)
%        end

        distance=mean(distances);
        distance=distance(1);
        %distance=sonarRead;
        %distance = (distance+sonarRead)/2;
        pause(0.005);
        buttonPressed=b.inputReadSI(0,Device.Port4,Device.Pushed);

        if (distance < MIN_DISTANCE_TO_WALL)
            speedLeft=DRIVESPEED/2;
            speedRight=-DRIVESPEED/2;
            distances(1:3)=distance; % reset array to drive back longer
        else
            speedLeft=DRIVESPEED+P*((MAX_DISTANCE_TO_WALL+MIN_DISTANCE_TO_WALL)/2-distance);
            speedRight=DRIVESPEED-P*((MAX_DISTANCE_TO_WALL+MIN_DISTANCE_TO_WALL)/2-distance);
        end

        [distance, P*((MAX_DISTANCE_TO_WALL+MIN_DISTANCE_TO_WALL)/2-distance) speedLeft speedRight]

        b.outputPower(0,Device.MotorB, speedLeft);
        pause(0.005);
        b.outputPower(0,Device.MotorC, speedRight);    
        pause(0.050);
        %pause(1);
    end
    b.outputStop(0,Device.MotorB+Device.MotorC,0)
    pause(0.5);
end