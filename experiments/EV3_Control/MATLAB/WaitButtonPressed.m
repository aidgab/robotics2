function [  ] = WaitButtonPressed(b)
%WaitButtonPressed waits for robot's button press on port 4
	while (b.inputReadSI(0,Device.Port4,Device.Pushed)==0)
        pause(0.05);
    end
end

