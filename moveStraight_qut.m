function [ output_args ] = moveStraight_qut(robot, speed, length)
    robot.outputPower(0,Device.MotorB,speed)
    robot.outputPower(0,Device.MotorC,speed)
    
    robot.outputClrCount(0,Device.MotorB);                    % Reset motor rotation counter
    robot.outputClrCount(0,Device.MotorC);

    PERIOD = 0.1;                               % Sampling period in seconds
    WHEEL_DIAM=0.056;                           % Diameter of wheel
    P = 0.1;                                   % P controller parameter
    I = 0.0;                                   % I controller parameter
    D = 0.0;                                   % D controller parameter

    robot.outputStart(0,Device.MotorB); %robot start
    pause
    robot.outputStart(0,Device.MotorC);
        
    lastRotationLeft = 0.0;
    lastRotationRight = 0.0;
    integral=0.0;

    distanceTravelled=0.0;
    prediff=0;
    leftMotorSpeed=speed;
    while (distanceTravelled<length)
        angleLeft = robot.outputGetCount(0,Device.MotorB)            % Read rotation counter in degrees
        angleRight = robot.outputGetCount(0,Device.MotorC)      
        % we try to keep speed of motor 2
        speedLeft = (angleLeft - lastRotationLeft)/PERIOD;          % Calculate the real speed in d/s
        speedRight = (angleRight - lastRotationRight)/PERIOD;

        diff = speedRight - speedLeft;                 % P controller

        % I controller
        integral=integral+PERIOD*diff;
        % D controller
        derivative=(diff-prediff)/PERIOD;

        prediff=diff;
        leftMotorSpeed=leftMotorSpeed + int8(diff * P) + int8(integral * I)+ int8(derivative * D)
        robot.outputSpeed(0,Device.MotorB,leftMotorSpeed);

        %assuming that right wheel works fine - calc distance 
        distanceTravelled=distanceTravelled+(double(angleRight - lastRotationRight)/180)*(WHEEL_DIAM/2)*pi;

        %save state
        lastRotationLeft = angleLeft;
        lastRotationRight = angleRight;

        pause(PERIOD);                          % Wait for next sampling period
    end
    
    robot.outputStopAll();                      % Stop motor 
end