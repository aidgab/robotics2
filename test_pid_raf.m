robot = legoev3('usb');                           % Set up MATLAB and EV3 communication
%robot = legoev3('bluetooth','com8');

% Change based on your motor port numbers
leftMotor = motor(robot, 'B');              % Set up motor
rightMotor = motor(robot, 'C');      


%moveStraight(robot,20,0.5)
rotate(robot, 90);
%moveStraight(robot,20,0.2)
%rotate(robot, 90);
%moveStraight(robot,20,0.2)
%rotate(robot, -180);

%while ~readButton(robot, 'up')    
%end
stop(leftMotor);                             % Stop motor 
stop(rightMotor);

clear