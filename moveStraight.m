function [ output_args ] = moveStraight(robot, speed, length)
    leftMotor = motor(robot, 'B');              % Set up motor
    rightMotor = motor(robot, 'C');      
    
    leftMotor.Speed=speed;
    rightMotor.Speed=speed;
    
    resetRotation(leftMotor);                    % Reset motor rotation counter
    resetRotation(rightMotor);

    PERIOD = 0.1;                               % Sampling period in seconds
    WHEEL_DIAM=0.056;                           % Diameter of wheel
    P = 0.1;                                   % P controller parameter
    I = 0.0;                                   % I controller parameter
    D = 0.0;                                   % D controller parameter
    start(leftMotor);                            % Start motor
    start(rightMotor);
    
    lastRotationLeft = 0.0;
    lastRotationRight = 0.0;
    integral=0.0;

    distanceTravelled=0.0;
    prediff=0;
    while (distanceTravelled<length)
        angleLeft = readRotation(leftMotor);            % Read rotation counter in degrees
        angleRight = readRotation(rightMotor);      
        % we try to keep speed of motor 2
        speedLeft = (angleLeft - lastRotationLeft)/PERIOD;          % Calculate the real speed in d/s
        speedRight = (angleRight - lastRotationRight)/PERIOD;

        diff = speedRight - speedLeft;                 % P controller

        % I controller
        integral=integral+PERIOD*diff;
        % D controller
        derivative=(diff-prediff)/PERIOD;

        prediff=diff;
        leftMotor.Speed = leftMotor.Speed + int8(diff * P) + int8(integral * I)+ int8(derivative * D);

        %assuming that right wheel works fine - calc distance 
        distanceTravelled=distanceTravelled+(double(angleRight - lastRotationRight)/180)*(WHEEL_DIAM/2)*pi;

        %save state
        lastRotationLeft = angleLeft;
        lastRotationRight = angleRight;

        pause(PERIOD);                          % Wait for next sampling period
    end
    
    stop(rightMotor);
    stop(leftMotor);                             % Stop motor 
end