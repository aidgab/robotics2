%% This is a collision avoidance app by Aidar
%% Set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

robot = legoev3('usb');                           % Set up MATLAB and EV3 communication
KEEP_RANGE = 0.5;                                % Warning - turn right
MAXSPEED = 50;
DELTA = 0.05
;
ERROR = 0.1;
%-------------------------------------------


sonar = sonicSensor(robot);             % Set up ultrasonic sensor
leftMotor = motor(robot, 'B');              % Set up motor
rightMotor = motor(robot, 'C');              


leftMotor.Speed=MAXSPEED ;
rightMotor.Speed=MAXSPEED ;
start(leftMotor);
start(rightMotor);

%% Operations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dis=0;

while ~readButton(robot, 'up')             % Exit if UP button is pressed
    newDis = readDistance(sonar)           % Read ultrasonic sensor value
    dis=(dis+newDis)/2;

    if ((dis>(KEEP_RANGE-DELTA)) & (dis<(KEEP_RANGE+DELTA)))
        leftMotor.Speed=0;
        rightMotor.Speed=0;
    elseif (dis < KEEP_RANGE)
        leftMotor.Speed=-MAXSPEED;
        rightMotor.Speed=-MAXSPEED;
        %writeLCD(robot, 'FULL DRIVE!',1,1);
    else
        leftMotor.Speed=MAXSPEED;
        rightMotor.Speed=MAXSPEED;
    end
    
    %battery state 
    if (robot.BatteryLevel<10)
        writeLCD(robot, 'LOW BATTERY. Please charge ;(',8,1);
    end
end

%% Clean up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop(leftMotor);
stop(rightMotor);

clear
