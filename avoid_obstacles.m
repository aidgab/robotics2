%% This is a collision avoidance app by Aidar
%% Set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

robot = legoev3('usb');                           % Set up MATLAB and EV3 communication
WARNING_RANGE = 0.5;                                % Warning - turn right
CRITICAL_RANGE = 0.15;                              % Critical - rotate
MAXSPEED = 100
%-------------------------------------------


sonar = sonicSensor(robot);             % Set up ultrasonic sensor
leftMotor = motor(robot, 'B');              % Set up motor
rightMotor = motor(robot, 'C');              


leftMotor.Speed=MAXSPEED ;
rightMotor.Speed=MAXSPEED ;
start(leftMotor);
start(rightMotor);

%% Operations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

while ~readButton(robot, 'up')             % Exit if UP button is pressed
    dis = readDistance(sonar);           % Read ultrasonic sensor value
    if (dis<CRITICAL_RANGE)
        %turning right
        leftMotor.Speed=-MAXSPEED/2;
        rightMotor.Speed=-MAXSPEED;
        writeLCD(robot, 'WOWOWOW! Lets get back!',1,1);
    elseif(dis<WARNING_RANGE)
        leftMotor.Speed=(dis/WARNING_RANGE)*100;
        rightMotor.Speed=leftMotor.Speed/4;
        writeLCD(robot, 'Careful, some obstacles on road',1,1);
    else
        leftMotor.Speed=MAXSPEED;
        rightMotor.Speed=MAXSPEED;
        writeLCD(robot, 'FULL DRIVE!',1,1);
    end
    
    %battery state 
    if (robot.BatteryLevel<10)
        writeLCD(robot, 'LOW BATTERY. Please charge ;(',8,1);
    end
end

%% Clean up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop(leftMotor);
stop(rightMotor);

clear
