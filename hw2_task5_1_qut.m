clear
%% This is a collision avoidance app by Aidar
%% Set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% init the connection
disp('Connecting ... ')
% brick usb init
b = Brick('ioType','usb');
%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
b.inputDeviceClrAll(0);

b.beep();

CRITICAL_RANGE = 0.5;                                % Warning - turn right
KEEP_RANGE = 0.2;                                % Warning - turn right
MAXSPEED = 50;
%-------------------------------------------


sonar = sonicSensor(robot);             % Set up ultrasonic sensor
Device.USDistCM

leftMotor = motor(robot, 'B');              % Set up motor
rightMotor = motor(robot, 'C');              


leftMotor.Speed=MAXSPEED ;
rightMotor.Speed=MAXSPEED ;
start(leftMotor);
start(rightMotor);

%% Operations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

while ~readButton(robot, 'up')             % Exit if UP button is pressed
    dis = readDistance(sonar);           % Read ultrasonic sensor value
    if (dis<KEEP_RANGE)
        %turning right
        t=(dis/KEEP_RANGE)*MAXSPEED
        leftMotor.Speed=0;
        rightMotor.Speed=0;
        %writeLCD(robot, 'WOWOWOW! Lets get back!',1,1);
    elseif (dis < CRITICAL_RANGE)
        leftMotor.Speed=dis/KEEP_RANGE*MAXSPEED;
        rightMotor.Speed=dis/KEEP_RANGE*MAXSPEED;
        %writeLCD(robot, 'FULL DRIVE!',1,1);
    else
        leftMotor.Speed=MAXSPEED;
        rightMotor.Speed=MAXSPEED;
    end
    
    %battery state 
    if (robot.BatteryLevel<10)
        writeLCD(robot, 'LOW BATTERY. Please charge ;(',8,1);
    end
end

%% Clean up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop(leftMotor);
stop(rightMotor);

clear
