% Copyright 2015 Aidar Gabdullin 
% that is fucking awesome. It works!


%% Set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%robot = legoev3('bluetooth', 'COM8');                           % Set up MATLAB and EV3 communication
robot = legoev3('usb');                           % Set up MATLAB and EV3 communication
MAXSPEED=50;
%-------------------------------------------

lightSensor = colorSensor(robot);
leftMotor = motor(robot, 'B');              % Set up motor
rightMotor = motor(robot, 'C');              

fprintf('Learning phase! Please place me on the white and press any key. \n');
pause 
white = readLightIntensity(lightSensor,'ambient');
fprintf('Now place me on the black and press ENTER. \n');
pause 
black = readLightIntensity(lightSensor,'ambient');
fprintf('Now Im ready to go! place me on left side of the black line and press any key. \n');
pause 


%%---Setup ------

%% Operations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

leftMotor.Speed=50;
rightMotor.Speed=50;
start(leftMotor);
start(rightMotor);

while ~readButton(robot, 'up')             % Exit if UP button is pressed
    intensity = readLightIntensity(lightSensor,'ambient');
    if (intensity < (white-black)/2+black)
        %more black - turning left
        if (intensity < (white-black)/4+black) %������ ����� - ����������� ������
            leftSpeed=MAXSPEED/4;
        else
            leftSpeed=MAXSPEED/2;
        end
        leftMotor.Speed=leftSpeed;
        rightMotor.Speed=MAXSPEED;
    else
        %more white- turning right
        if (intensity > white-(white-black)/4) %������ ����� - ����������� ������
            rightSpeed=MAXSPEED/4;
        else
            rightSpeed=MAXSPEED/2;
        end

        leftMotor.Speed=MAXSPEED;
        rightMotor.Speed=rightSpeed;        
    end
    
    %maybe we need to make a pause here - too much sampling
end

%% Clean up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop(leftMotor);
stop(rightMotor);

clear
