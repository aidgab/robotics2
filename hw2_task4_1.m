robot = legoev3('usb');                           % Set up MATLAB and EV3 communication
%robot = legoev3('bluetooth', 'COM7');

% Change based on your motor port numbers
leftMotor = motor(robot, 'B');              % Set up motor
rightMotor = motor(robot, 'C');      

moveStraight(robot,30,1)
rotate(robot,-76)
moveStraight(robot,30,0.8)
rotate(robot,-76)

moveStraight(robot,30,1)
rotate(robot,-76)
moveStraight(robot,30,0.8)
rotate(robot,-76)

%while ~readButton(robot, 'up')    
%end
stop(leftMotor);                             % Stop motor 
stop(rightMotor);

clear