% this is the implementation of following a triangular trajectory by
% masters team
% Implementation of HW 2 task 6

disp('Connecting ... ')
% brick usb init
%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
b = Brick('ioType','usb');
b.inputDeviceClrAll(0);
%clearing sensor buffers
ang=readGyroAngle(b);

disp('Connected to robot!')
%b.playThreeTones();
%b.beep();

%path 1
moveStraight_qut(b,5,0.1);
pause(1);
moveStraight_qut(b,5,0.001);
pause(1);
rotate_qut(b,90);
pause(1);
moveStraight_qut(b,5,0.001);
pause(1);

%moveStraight_qut(b,50,0.3);
%rotate_qut(b,180);
clear