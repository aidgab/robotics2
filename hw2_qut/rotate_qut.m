function [ output_args ] = rotate_qut(robot, angle)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    %resetRotationAngle(gyro);
    startAngle=readGyroAngle(robot)

    speed=10;
    
    robot.outputStart(0,Device.MotorB+Device.MotorC); %robot start
    pause(0.001);
    robot.outputStart(0,Device.MotorC);        

    rotatedAngle=startAngle;
    while (abs(rotatedAngle-startAngle))<abs(angle)
        if (abs(rotatedAngle-startAngle)>abs(angle)-20)
            speed=10;
        end
        
        %robot.outputStepSpeed(0,Device.MotorB,sign(angle)*speed,0,720,0,Device.Brake)
        robot.outputPower(0,Device.MotorB,sign(angle)*speed)
        pause(0.001);
        %robot.outputStepSpeed(0,Device.MotorC,-sign(angle)*speed,0,720,0,Device.Brake)
        robot.outputPower(0,Device.MotorC,-sign(angle)*speed)
        pause(0.001);
        
        rotatedAngle=readGyroAngle(robot)
    end
    
    robot.outputStopAll();
end