function [ output_args ] = moveStraight_qut(robot, speed, length)
    robot.outputPower(0,Device.MotorB,speed)
    robot.outputPower(0,Device.MotorC,speed)
    
    WHEEL_DIAM=0.056;
    
    angle=(length/(WHEEL_DIAM*pi))*360;
    robot.outputStepSpeed(0,Device.MotorB+Device.MotorC,speed,0,angle,0,Device.Brake)
    while(robot.outputTest(0,Device.MotorB))
        pause(0.1)
    end
    % wait until motor B has moved
    while(robot.outputTest(0,Device.MotorC))
        pause(0.1)
    end
end