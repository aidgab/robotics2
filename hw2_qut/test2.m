disp('Connecting ... ')
% brick usb init
%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
b = Brick('ioType','usb');
angleA = [-10;-20;30;40;50];
speedA = 20;
angleB = [50;40;30;-20;-10];
speedB = 10;
for ii=1:length(angleA)
    % move motor A
    b.outputStepSpeed(0,Device.MotorB,sign(angleA(ii))*speedA,0,angleA(ii),0,Device.Brake)
    % move motor B
    b.outputStepSpeed(0,Device.MotorB,sign(angleB(ii))*speedB,0,angleB(ii),0,Device.Brake)
    % wait until motor A has moved
    while(b.outputTest(0,Device.MotorB))
        pause(0.1)
    end
    % wait until motor B has moved
    while(b.outputTest(0,Device.MotorB))
        pause(0.1)
    end
end