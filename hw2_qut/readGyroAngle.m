function [ angle ] = readGyroAngle( robot )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    angle=robot.inputReadSI(0,Device.Port3,Device.GyroAng);
    i=10;
    while(isnan(angle) && (i>0))
        %display('Gyro angle is NaN');
        pause(0.005);
        angle=robot.inputReadSI(0,Device.Port3,Device.GyroAng)
        i=i-1;
    end
end

