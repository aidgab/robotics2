% init the connection
disp('Connecting ... ')
% brick usb init
b = Brick('ioType','usb');
b.inputDeviceClrAll(0);

%b = Brick('ioType','instrbt','btDevice','MASTERS_EV3','btChannel',1)
% beep to indicate connection
b.beep();

WHEEL_DIAM=0.056;
WHEEL_S=WHEEL_DIAM*pi;
i=0
while i<1
    b.outputStepSpeed(0,Device.MotorB,20,0,(0.21/WHEEL_S)*360,0,Device.Brake)
    b.outputStepSpeed(0,Device.MotorC,20,0,(0.21/WHEEL_S)*360,0,Device.Brake)

    i=i+1;
    pause;
end
clear