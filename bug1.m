%% This is a implementation of bug algorithm by Aidar
%% Set up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

WARNING_RANGE = 0.5;                                % Warning - turn right
CRITICAL_RANGE = 0.15;                              % Critical - rotate
MAXSPEED = 100
PRECISION=0.05; % precision of distance
%-------------------------------------------

% You should now place a robot on a (0,0) point and define a goal (in
% meters)
x=0;
y=0;
goalX = input('Define a goal x in meters:');
goalY = input('Define a goal y in meters:');
[goalX goalY]

fprintf('Press any key to start movement. \n');
pause 


%% Operations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
inPlace=((abs(goalX-x)<PRECISION) && (abs(goalY-y)<PRECISION)); %% achieved goal
while ~inPlace
    
end

fprintf('Reached the goal. Yaaaaar! \n');

%% Clean up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
