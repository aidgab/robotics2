function [ output_args ] = rotate(robot, angle)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    leftMotor = motor(robot, 'B');              % Set up motor
    rightMotor = motor(robot, 'C');      
    gyro = gyroSensor(robot);
    resetRotationAngle(gyro);

    pause(0.1);
    startAngle=readRotationAngle(gyro); 

    speed=20;
    direction=1;
    if (angle<0)
        direction=-1;
    end
    
    start(leftMotor);                            % Start motor
    start(rightMotor);

    rotatedAngle=readRotationAngle(gyro)
    while (abs(rotatedAngle-startAngle))<=abs(angle)-1
        if (abs(rotatedAngle-startAngle)>abs(angle)-20)
            speed=10
        end
        leftMotor.Speed=direction*speed;
        rightMotor.Speed=-direction*speed;        
        %readRotationRate(gyro)
        rotatedAngle=readRotationAngle(gyro);
    end
    
    stop(rightMotor);
    stop(leftMotor);                             % Stop motor 
end

