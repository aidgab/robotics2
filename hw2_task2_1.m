% this exercise drives the robot by circular trajectory
% this is an open loop solution of 

CIRCLE_RADIUS=1; % 0.5 meters
WHEEL_BASE=0.12; %wheel base in meters
MAX_SPEED=80; % max speed in percents

mylego = legoev3('usb');                           % Set up MATLAB and EV3 communication

% Change based on your motor port numbers
leftMotor = motor(mylego, 'B');              % Set up motor
rightMotor = motor(mylego, 'C');      

%make some calculations
leftMotor.Speed=MAX_SPEED;
rightMotor.Speed=leftMotor.Speed*(1+WHEEL_BASE/CIRCLE_RADIUS);

start(leftMotor);                            % Start motor
start(rightMotor);

while ~readButton(mylego, 'up')

end
stop(leftMotor);                             % Stop motor 
stop(rightMotor);

clear