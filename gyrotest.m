robot = legoev3('usb');                           % Set up MATLAB and EV3 communication
gyroSensor = gyroSensor(robot);
resetRotationAngle(gyroSensor);

while ~readButton(robot, 'up')             % Exit if UP button is pressed
    readRotationAngle(gyroSensor)
end
clear