function [ output_args ] = rotate_qut(robot, angle)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    %resetRotationAngle(gyro);
    startAngle=robot.inputReadSI(0,Device.Port3,Device.GyroAng);

    speed=20;
    direction=1;


    if (angle<0)
        direction=-1;
    end
    
    robot.outputStart(0,Device.MotorB); %robot start
    robot.outputStart(0,Device.MotorC);
    
    rotatedAngle=NaN
    while (isnan(rotatedAngle))
        rotatedAngle=robot.inputReadSI(0,Device.Port3,Device.GyroAng)
    end
    
    robot.outputPower(0,Device.MotorB,speed)
    robot.outputPower(0,Device.MotorC,speed)

    pause
    while (abs(rotatedAngle-startAngle))<=abs(angle)-1
        if (abs(rotatedAngle-startAngle)>abs(angle)-20)
            speed=10;
        end
        
        robot.outputPower(0,Device.MotorB,direction*speed)
        robot.outputPower(0,Device.MotorC,-direction*speed)
        
        %readRotationRate(gyro)
        rotatedAngle=robot.inputReadSI(0,Device.Port3,Device.GyroAng)
        while (isnan(rotatedAngle))
            rotatedAngle=robot.inputReadSI(0,Device.Port3,Device.GyroAng)
            pause(0.1)
        end

        pause(0.5);
    end
    
    robot.outputStopAll();
end

